# Return a random decimal number between <lower> and <upper>.
# The bounds are exclusive, meaning they should never be returned from this method.
import random


def random_in_range(lower, upper):
    randomnumber = random.uniform(lower, upper)
    print(randomnumber)
    return randomnumber


def test_random_in_range():
    assert 1 < random_in_range(1, 10) < 10
    assert 50 < random_in_range(50, 1000) < 1000
    assert 1 < random_in_range(1, 2) < 2
    assert 300 < random_in_range(300, 1000000) < 1000000
    assert 3 < random_in_range(3, 5) < 5
    assert 110022 < random_in_range(110022, 111000222) < 111000222





