# Deklan Glaser
# Return the sum of all primes under n.
import threading
import math
global bucket1
global bucket2
global bucket4
global bucket5
global bucket3
global bucket6
global bucket7
global bucket8
global bucket9
global bucket10
bucket1 = []
bucket2 = []
bucket4 = []
bucket5 = []
bucket3 = []
bucket6 = []
bucket7 = []
bucket8 = []
bucket9 = []
bucket10 = []
def isPrime(num):
    a=2
    while a<=math.sqrt(num):
        if num%a<1:
            return False
        a=a+1
    return num>1

def thread1():
    bucket = bucket1
    global total1
    total1 = 0
    print("Thread 1 Started")
    for i in bucket:
        if isPrime(i):
            print(f"Thread 1 Prime {i}")
            total1 += i
    return

def thread2():
    bucket = bucket2
    global total2
    total2 = 0
    print("Thread 2 Started")
    for i in bucket:
        if isPrime(i):
            print(f"Thread 2 Prime {i}")
            total2 += i
    return

def thread3():
    bucket = bucket3
    global total3
    total3 = 0
    print("Thread 3 Started")
    for i in bucket:
        if isPrime(i):
            print(f"Thread 3 Prime {i}")
            total3 += i
    return


def thread4():
    bucket = bucket4
    global total4
    total4 = 0
    print("Thread 4 Started")
    for i in bucket:
        if isPrime(i):
            print(f"Thread 4 Prime {i}")
            total4 += i
    return

def thread5():
    bucket = bucket5
    global total5
    total5 = 0
    print("Thread 5 Started")
    for i in bucket:
        if isPrime(i):
            print(f"Thread 5 Prime {i}")
            total5 += i
    return

def thread6():
    bucket = bucket6
    global total6
    total6 = 0
    print("Thread 6 Started")
    for i in bucket:
        if isPrime(i):
            print(f"Thread 6 Prime {i}")
            total6 += i
    return

def thread7():
    bucket = bucket7
    global total7
    total7 = 0
    print("Thread 7 Started")
    for i in bucket:
        if isPrime(i):
            print(f"Thread 7 Prime {i}")
            total7 += i
    return

def thread8():
    bucket = bucket8
    global total8
    total8 = 0
    print("Thread 8 Started")
    for i in bucket:
        if isPrime(i):
            print(f"Thread 8 Prime {i}")
            total8 += i
    return

def thread9():
    bucket = bucket9
    global total9
    total9 = 0
    print("Thread 9 Started")
    for i in bucket:
        if isPrime(i):
            print(f"Thread 9 Prime {i}")
            total9 += i
    return

def thread10():
    bucket = bucket10
    global total10
    total10 = 0
    print("Thread 10 Started")
    for i in bucket:
        if isPrime(i):
            print(f"Thread 10 Prime {i}")
            total10 += i
    return
def sum_primes_under(n):
    # Assign numbers evenly to buckets
    for i in range(n):
        if len(bucket1) == 0:
            bucket1.append(i)
        elif len(bucket1) < len(bucket2):
            bucket1.append(i)
        elif len(bucket2) < len(bucket3):
            bucket2.append(i)
        elif len(bucket3) < len(bucket4):
            bucket3.append(i)
        elif len(bucket4) < len(bucket5):
            bucket4.append(i)
        elif len(bucket5) < len(bucket6):
            bucket5.append(i)
        elif len(bucket6) < len(bucket7):
            bucket6.append(i)
        elif len(bucket7) < len(bucket8):
            bucket7.append(i)
        elif len(bucket8) < len(bucket9):
            bucket8.append(i)
        elif len(bucket9) < len(bucket10):
            bucket9.append(i)
        else:
            bucket10.append(i)

    p1 = threading.Thread(target=thread1, name="Python Prime Number Addition Bucket #1")
    p2 = threading.Thread(target=thread2, name="Python Prime Number Addition Bucket #2")
    p3 = threading.Thread(target=thread3, name="Python Prime Number Addition Bucket #3")
    p4 = threading.Thread(target=thread4, name="Python Prime Number Addition Bucket #4")
    p5 = threading.Thread(target=thread5, name="Python Prime Number Addition Bucket #5")
    p6 = threading.Thread(target=thread6, name="Python Prime Number Addition Bucket #6")
    p7 = threading.Thread(target=thread7, name="Python Prime Number Addition Bucket #7")
    p8 = threading.Thread(target=thread8, name="Python Prime Number Addition Bucket #8")
    p9 = threading.Thread(target=thread9, name="Python Prime Number Addition Bucket #9")
    p10 = threading.Thread(target=thread10, name="Python Prime Number Addition Bucket #10")

    p1.start()
    p2.start()
    p3.start()
    p4.start()
    p5.start()
    p6.start()
    p7.start()
    p8.start()
    p9.start()
    p10.start()

    p1.join()
    p2.join()
    p3.join()
    p4.join()
    p5.join()
    p6.join()
    p7.join()
    p8.join()
    p9.join()
    p10.join()
    total = total1 + total2+ total3 + total4 + total5 + total6 + total7 + total8 + total9 + total10
    print(total)
    return total


def test_is_prime():
    assert sum_primes_under(100000) == 454396537