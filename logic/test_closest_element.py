# You are given a collection of elements, a single <center> element, and a <check> element.
# Return the index at which a <check> element is closest to a <center> element.
# For example: given func([3, 1, 1, 2, 3], 2, 3), we are looking for the index of a <3> element closest to a <2> element
# The correct answer is 4, because the 5th element (zero-index) is 1 slot away from a 2.
# The 0th index value is also 3, but it is further away from the center 2, so "0" is an incorrect answer.

# BONUS POINTS: right now we assume there is only 1 center value.
# What if the input has multiple center and check values?
def find_closest_element(collection, center, check):
    checklist = []
    centerlist = []

    # Populates 2 lists with the index locations of the check and center values throughout the collection list.
    for i in range(len(collection)):
        if collection[i] == check:
            checklist.append(i)
        if collection[i] == center:
            centerlist.append(i)

    # Verifies that both the check and center values were found at least once within the collection list.
    if len(checklist) > 0 and len(centerlist) > 0:
        diff = abs(centerlist[0] - checklist[0])
        checkindex = checklist[0]
    else:
        return -1

    # Cycles through the two lists with the index locations and determines which two location has the smallest 
    # difference
    for i in range(1, len(checklist)):
        if i < len(centerlist) and abs(checklist[i] - centerlist[i]) < diff:
            diff = abs(checklist[i] - centerlist[i])
            checkindex = checklist[i]

    return checkindex


def test_find_closest_element():
    # New unit tests:
    assert find_closest_element([3, 6, 5, 7, 8, 3, 5, 77, 9, 3], 3, 5) == 6
    assert find_closest_element([1, 1, 1, 1, 1, 1, 1, 1, 2, 5, 2, 1, 1, 1, 1, 1, 5], 5, 2) == 8

    # Original unit tests:
    assert find_closest_element([1, 2, 3, 4, 5, 6, 7, 8, 9, 7], 7, 4) == 3

    # "2" element closest to a "1" element, last (7th) index is closest
    assert find_closest_element([1, 0, 0, 2, 0, 0, 1, 2], 1, 2) == 7
