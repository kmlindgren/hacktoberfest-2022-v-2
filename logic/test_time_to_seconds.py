# Given a string in the format "hours:minutes:seconds", return the total number of seconds represented by the time.
def time_to_seconds(string):
    # Define the variables.
    count = 0
    hours = minutes = seconds = ""

    # Run through the string, separating out the hours, minutes, and seconds.
    for i in string:
        if i == ':':
            count += 1
        elif count == 0:
            hours += i
        elif count == 1:
            minutes += i
        else:
            seconds += i

    # Return the total number of seconds as an integer.
    return int(hours) * 3600 + int(minutes) * 60 + int(seconds)


def test_time_to_seconds():
    # Added unit tests:
    assert time_to_seconds("24:15:34") == 87_334
    assert time_to_seconds("100:00:100") == 360_100
    assert time_to_seconds("00:00:00") == 0
    # Original unit tests:
    assert time_to_seconds("1:00:00") == 3_600
    assert time_to_seconds("1:00:10") == 3_610
    assert time_to_seconds("2:10:00") == 7_800
