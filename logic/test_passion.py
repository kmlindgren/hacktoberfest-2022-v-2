# You must evaluate the "passion level" of the input string, which is a numerical rating of how passionate
# the person posting it is. The rules are as follows:
# >50% capital letters is 1 point
# 1 or more exclamation point is 1 point
# 5 or more exclamation points is an additional 1 point
# All rules can be combined for a maximum of up to 3 points
# BONUS CHALLENGE: any input string ending with "..." gains minus 1 point
def get_passion_level(check):
    passion = getUpperAndExclamationPoints(check) + getEllipsesPoints(check)
    return passion

def getUpperAndExclamationPoints(statement):
    upper = exclamations = total = points = 0

    for char in statement:
        if char.isupper():
            upper += 1
        if char == '!':
            exclamations += 1
        if char.isalpha():
            total += 1

    if upper / total > 0.5:
        points += 1
    if exclamations >= 1:
        points += 1
    if exclamations >= 5:
        points += 1

    return points

def getEllipsesPoints(statement):
    points = 0
    if statement[-1] == statement[-2] == statement[-3] == '.':
        points -= 1
    return points

def test_passion_level():
    # original unit tests
    assert get_passion_level("woohoo") == 0
    assert get_passion_level("Woohoo") == 0
    assert get_passion_level("WOOHOO") == 1
    assert get_passion_level("woohoo!") == 1
    assert get_passion_level("WOOHOO!") == 2
    assert get_passion_level("WOOHOO!!!!!") == 3
    assert get_passion_level("woohoo...") == -1

    # new unit tests
    assert get_passion_level("i LoVe! PyThON!!!!") == 3
    assert get_passion_level("I LOVE PYTHON...!") == 2
    assert get_passion_level("i love python!!!!!") == 2
    assert get_passion_level("I! LOVE! PYTHON!!!...") == 2
    assert get_passion_level("123456789... PYTHON COOL") == 1
