# Given an input integer, return the product of all digits within that integer.
# Example: product_digits(123) => 6, because 1 * 2 * 3 == 6.
def product_digits(string):
    string = str(string)
    product = 1
    for char in string:
        product *= int(char)
    return product

def test_product_digits():
    assert product_digits(123456789) == 362880
    assert product_digits(54545) == 2000
