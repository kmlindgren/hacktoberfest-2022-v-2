# Convert each integer part into a spoken string representation.
# Challenge: don't make an if-statement for every integer -> string case (hint: dictionary)
def integer_to_words(int_in):
    str_int = str(int_in)
    translate_dict = {'1':'one ','2':'two ','3':'three ','4':'four ','5':'five ','6':'six ','7':'seven ','8':'eight ','9':'nine ','0':'zero '}
    decoded = ""
    for n in str_int:
        decoded += translate_dict[n]

    return decoded.rstrip()


def test_integer_to_words():
    assert integer_to_words(123) == "one two three"
    assert integer_to_words(9874) == "nine eight seven four"

    assert integer_to_words(999) == "nine nine nine"
    assert integer_to_words(1000) == "one zero zero zero"
    assert integer_to_words(16) == "one six"
    assert integer_to_words(10020) == "one zero zero two zero"
