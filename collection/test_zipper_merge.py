# Zipper merge collections <a> and <b>, and assume they are the same length / non-empty. Start with <a>.
def zipper_merge(a, b):
    list1 = ["a", "b", "c"]
    list2 = [1, 2, 3]
    list3 = [item for sublist in zip(list1, list2) for item in sublist]

    return list3

def test_zipper_merge():
    assert zipper_merge(["a", "b", "c"], [1, 2, 3]) == ["a", 1, "b", 2, "c", 3]
