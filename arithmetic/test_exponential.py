# Returns the result of x to the power of y.
def pow_numbers(x, y):
    return pow(x, y)

def test_pow_numbers():
    assert pow_numbers(6, 3) == 216

def test_pow_numbers2():
    assert pow_numbers(2, 2) == 4

def test_pow_numbers3():
    assert pow_numbers(10, 5) == 100000

def test_pow_numbers4():
    assert pow_numbers(50, 5) == 312500000
