# Given an angle in degrees, return the radian equivalent.
import math


def degrees_to_radians(degrees):
    degrees = (degrees/360) * (2 * math.pi)
    return degrees


def test_degrees_to_radians_360():
    # Round before comparing, because some of these numbers are irrational :)
    assert round(degrees_to_radians(360), 4) == 6.2832


def test_degrees_to_radians_180():
    assert round(degrees_to_radians(180), 4) == 3.1416


def test_degrees_to_radians_90():
    assert round(degrees_to_radians(90), 4) == 1.5708

